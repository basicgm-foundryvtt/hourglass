A module for [Foundry VTT](https://foundryvtt.com/) which enables the GM to display an hourglass-style countdown timer to all players.
